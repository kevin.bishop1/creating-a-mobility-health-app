//
//  NetworkTests.swift
//  SmoothWalkerUnitTests
//
//  Created by Kevin Bishop on 4/13/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import XCTest
import HealthKit
@testable import SmoothWalker

final class NetworkTests: XCTestCase {

    func testExample() throws {

    }

    private func createMockSamplesHelper() -> [HKObject] {
        let semaphore = DispatchSemaphore(value: 0)
        var serverHealthSamples: [ServerHealthSample] = []
        Network.pull() { serverResponse in
            serverHealthSamples = serverResponse.weeklyReport.samples
            semaphore.signal()
        }
        semaphore.wait()

        return serverHealthSamples.map{ try! Network.createHealthSample($0) }

    }

}
